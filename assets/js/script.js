
// Les arrays
let StudentsNames = ['Zoubaïr AHARBIL', 'Julien BAYLAC', 'Delphine BRUNETIERE', 'Alice CARRY', 'Honorin CHEVALIER','Ann-Katell DROUMAGUET','Audrey GONTHIER','Nora HENNEBERT','Quentin JUHEL','Carol LARQUETOUX','Emmanuelle LENORMAND','Jean-Marc MPOY','Maëva NAGOU',
'Régis NICOLO','Martin PELTIER','Philippe QUENTEL','Ewilan RIVIERE', 'Gaëtan SAVE', 'Hugo SCHINDELMAN','Maysam SHAFAQ-ZADAH','Meuslem SHAFAQ-ZADAH','Lionel SIMONETTI','Joanna TAS'];

let StudentsAges = [26,27,28,25,25,49,28,19,25,22,23,25,28,33,24,28,26,21,25,20,20,36,39];

let StudentsPhones = ['06 50 41 24 32','06 78 67 99 13','02 45 78 95 24','06 45 78 95 24','05 24 45 87 45','02 43 54 54 24','07 60 34 47 84','06 68 13 07 57','06 12 56 78 93','05 24 45 87 45','06 20 57 65 96','06 64 41 85 44','06 10 48 83 52',
'06 99 34 58 18','06 85 84 53 60','06 63 68 12 13','06 73 15 47 25','06 88 58 11 26','06 63 18 22 10','06 06 85 27 79','06 74 72 65 60','06 63 25 97 18','06 31 47 22 29'];

let StudentsAdresses = ['La Rouairie','11 square les hautes ourmes','14 avenue de la coquillette cendrée', '18 rue du Puits Jacob', '15 avenue de la ciboulette','7 avenue du Gros Malhon',
'4 rue du Clos Sévigné','6 rue du Bobéril','42 La Ville Goudelin','12 rue des dauphins','5 Place du Landrel','22 Square Professeur Louis Antoine','81 rue Adolphe Leray','9, rue de Rennes',
'137 Avenue Aristide Briand','1 Rue Porcon de la Barbinais','14 La Boutonnerie','5 Place du Landrel','2 rue de la Vigne', '21 boulevard Charles Péguy', '21 boulevard Charles Péguy','7 bd louis Volclair',
'2 avenue du Canada'];

let StudentsCities = ['53410 le Bourgneuf la Foret','35000 Rennes','35000 Rennes','35000 Rennes','35000 Rennes','35000 Rennes',
'35510 Cesson-Sévigné','35200, Rennes', '22100 Dinan','35200 Noyal Chatillon','35200 Rennes','35000 Rennes','35000 Rennes','35250 Saint-Aubin d’Aubigné','35000 Rennes','35000 Rennes','35340 Liffré','22630 Les Champs-Géraux','35240 Le Rheu',
'35000 Rennes', '35000 Rennes', '35000 Rennes','35200 Rennes'];

let StudentsCitations = ['“Un film, c’est toujours une tentative, jamais une finalité.”','“Un film doit être comme un caillou dans une chaussure.”','“J’entreprend chaque film comme on entreprend un voyage.”','“Cinématographe, art militaire. Préparer un film comme une bataille.”','“Un film est un rêve mis en images.”','“À quoi bon faire un film sans enthousiasme ?”',
'“J’ai été hier au cinéma. Voir un film en noir et blanc. Pas un film en couleurs : je suis en deuil.”','“Tournons quelques scènes hors sujet. Je voudrais gagner l’Oscar du meilleur film étranger.”','“Un film, cest une psychothérapie très chère que les studios ne comprennent pas toujours.”',
'“Ce film était tellement mauvais que les gens faisaient la queue pour sortir.”','“Il faut tourner chaque film comme si cétait le dernier.”','“Pour faire un film, premièrement, une bonne histoire, deuxièmement, une bonne histoire, troisièmement, une bonne histoire.”','“Il na pas inventé le film à couper le souffle !”',
'“Chaque film est l’émanation d’une vision personnelle de la réalité.”','“La réalisation dun film, cest un pari.”','“Si mon nom donne plus de poids au film, tant mieux !”','“Chaque film revient à faire marcher une armée en bon ordre. ”','“Le film a la capacité de tout exprimer sans rien dire.”','“Chaque film correspond à un moment et doit être à chaque fois une absolue nécessité.”',
'“Un film n’est pas une tranche de vie, c’est une tranche de gâteau. ”',"“C'est dur de faire un film, mais travailler pour de bon, c'est pire !”",'“Quand on commence à tourner un film, on doit toujours se poser la question de pourquoi. ”',
'“La mort nous apprend à vivre et tout film, tout roman, tout oeuvre dart participe de la mort.”'];

let StudentsCitationsWitters = ['François Ozon','Lars von Trier ','André Téchiné','Robert Bresson','Aaron Eckhart','Jean Rochefort',
'Raymond Devos','Billy Wilder','Tim Burton','Pierre Doris','Ingmar Bergman','Henri-Georges Clouzot','Jacques Pater','Emir Kusturica',
'Etgar Keret','Sophie Marceau','Franco Zeffirelli','Marc Ferro','Martin Valente','Alfred Hitchcock ','Woody Allen',' Ai Weiwei','Gérard Guégan'];

let StudentsImages = ['./assets/images/DSC_7934.jpg','./assets/images/DSC_7983.jpg','./assets/images/DSC_7979.jpg','./assets/images/DSC_7962.jpg','./assets/images/DSC_7930.jpg','./assets/images/DSC_7971.jpg',
'./assets/images/DSC_7944.jpg','./assets/images/DSC_7966.jpg','./assets/images/DSC_7932.jpg','./assets/images/DSC_7985.jpg','./assets/images/DSC_7960.jpg','./assets/images/DSC_7976.jpg','./assets/images/DSC_7974.jpg',
'./assets/images/DSC_7968.jpg','./assets/images/DSC_7948.jpg','./assets/images/DSC_7953.jpg','./assets/images/DSC_7946.jpg','./assets/images/DSC_7956.jpg','./assets/images/DSC_7958.jpg','./assets/images/DSC_7940.jpg',
'./assets/images/DSC_7936.jpg','./assets/images/DSC_7950.jpg','./assets/images/DSC_7981.jpg'];

let StudentsGender = ['#masculin','#masculin','#feminin','#feminin','#masculin','#feminin','#feminin','#feminin','#masculin','#feminin','#feminin','#masculin',
'#feminin','#masculin','#masculin','#masculin','#feminin','#masculin','#masculin','#masculin','#masculin','#masculin','#masculin'];

let StudentsDistance = ['#5km','#5-10km','#10km','#10km','#5-10km','#10km','#5-10km','#5km','#5km','#5km','#10km','#10km','#5km','#5km','#5-10km','#10km','#10km','#10km','#5km','#10km','#5-10km','#10km'];

// Fonctions 
var globalNumber = $( "#students" ).children().length;
$( "#studentsConcernedNumber" ).html(globalNumber);

$('#btn-toggle-masculin').on('click', function() {
    this.classList.add('btn-info');
    this.classList.remove('btn-light');
    var elems = $('.masculin');
    for (element of elems) {
        element.classList.remove('border-white');
        element.classList.add('border-info');
    }
    updateNumberOfStudentsConcerned();
});

$('#btn-toggle-feminin').on('click', function() {
    this.classList.add('btn-info');
    this.classList.remove('btn-light');
    var elems = $('.feminin');
    for (element of elems) {
        element.classList.remove('border-white');
        element.classList.add('border-info');
    }
    updateNumberOfStudentsConcerned();
});

$('#btn-reset').on('click', function() {
    toggleClassFromClassName('border-info','border-white');
    toggleClassFromClassName('btn-info','btn-light');
    $('#studentsConcernedNumber').html(globalNumber);
});

var thumbnails = $('img');
    
for (thumbnail of thumbnails) {
    thumbnail.addEventListener('mouseover', function() {
        this.setAttribute('src',this.getAttribute('src').replace('.jpg','-alt.jpg'));
    });
    thumbnail.addEventListener('mouseout', function() {
        this.setAttribute('src',this.getAttribute('src').replace('-alt.jpg','.jpg'));
    });
}

function toggleClassFromClassName(fromClassName, toClassName) {
    var elems = $('.'+fromClassName);
    for (element of elems) {
        element.classList.remove(fromClassName);
        if (toClassName) {
            element.classList.add(toClassName);
        }
    }
}

function updateNumberOfStudentsConcerned() {
    $('#studentsConcernedNumber').html($('.border-info').length);
}

// Modale 
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus');
})

// Maj de la modale
function infos(a){
    $('#StudentsNames').html(StudentsNames[a]);
    $('#StudentsAges').html(StudentsAges[a]);
    $('#StudentsPhones').html(StudentsPhones[a]);
    $('#StudentsAdresses').html(StudentsAdresses[a]);
    $('#StudentsCities').html(StudentsCities[a]);
    $('#StudentsCitations').html(StudentsCitations[a]);
    $('#StudentsCitationsWitters').html(StudentsCitationsWitters[a]);
    $('#StudentsGender').html(StudentsGender[a]);
    $('#StudentsDistance').html(StudentsDistance[a]);
    $('#StudentsImages').attr('src',StudentsImages[a]);
    $('#StudentsMaps').attr('href', 'https://www.google.fr/maps/place/'+StudentsAdresses[a]+' '+StudentsCities[a]);
}
